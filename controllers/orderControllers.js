const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");



// Creating order
module.exports.addOrder = (req,res) => {

    if(req.user.isAdmin){

    res.send(true)

    } else {

    let newOrder = new Order({
        userId: req.user.id,
        receiver: req.body.receiver,
        contactNumber: req.body.contactNumber,
        shippingAddress: req.body.shippingAddress,
        productId: req.body.productId,
        quantity: req.body.quantity,
        totalAmount: req.body.totalAmount

    })
    
    newOrder.save()
    .then(result => res.send({message: "Your order has been successfully placed!"}))
    .catch(error => res.send(error))
}
};


// View Authenticated User Order
module.exports.getUserOrder = (req,res)=>{

    Order.find({userId:req.user.id})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."})
        } else {
            return res.send(result)
        }
    })
    .catch(error => res.send(error))

}

// View All Orders
module.exports.showAllOrders = (req,res)=>{

    Order.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error));


}


// View A Specific User Order/s
module.exports.showUserOrder = (req,res)=>{

    Order.find({userId:req.params.userId},{_id:0,totalAmount:1,products:1})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."});
        } else {
            return res.send(result);
        }
    })
    .catch(error => res.send(error));


}

// View A Specific User Order
module.exports.showOrder = (req,res) =>{

    Order.find({_id:req.params.orderId})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."});
        } else {
            return res.send(result);
        }
    })
    .catch(error => res.send(error));
}

// View Products Per Order
module.exports.showProductsPerOrder = (req,res) =>{

    Order.find({_id:req.params.orderId},{_id:0,userId:1})
    .then(foundUser => {
        let result = foundUser.map(a => a.userId);
        if (result[0] !== req.user.id) {
            return res.send({message: "Wrong User"})
        } else {
            Order.find({_id:req.params.orderId},{products:1})
            .then(result => {
                if (result === null){
                    return res.send({message: "No Orders Found."});
                } else {
                    return res.send(result);
                }
            })
            .catch(error => res.send(error));           
        }
    })
    .catch(error => res.send(error));

}
