// [SECTION] DEPENDENCIES
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// [SECTION] ROUTES

// register user
router.post('/', userControllers.registerUser);

// get all user
router.get('/allUser', userControllers.getAllUsers);

//login user
router.post('/login', userControllers.loginUser);

// get user details
router.get('/getUserDetails', verify, userControllers.getUserDetails)

// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

// updating user details
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

// set admin
router.patch('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

module.exports = router;
